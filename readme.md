# Readme

## About matfiles

**matfiles** is a Python code to perform mathematical operations on one or several ASCII files containing numerical data. Data in files can be added, subtracted, divided, as well as factors and constant values. Interpolation and integration can also be performed. 

## Installation

Requirements:
1. Python 3.0 and higher versions
2. Numpy
3. Scipy

## Licence

This code is under GNU General Public License (GPL) version 3
