#!/usr/bin/python3

import sys, os

from scipy import interpolate
from scipy.interpolate import interp1d
from scipy.integrate import simps, quad
from lib.tuttilib import sequify, sloadtxt, getnlnc
import numpy as np
import operator
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("opnames", type=str, nargs='+', help="op1 file1 op2 file2 op3 x,...")
parser.add_argument("-s","--shifts", type=str, help="shift1, shift2,...")
parser.add_argument("-x","--xfacts", type=str, help="xfact1, xfact2,...")
parser.add_argument("-a","--adds", type=str, help="add1, add2,...")
parser.add_argument("-f","--facts", type=str, help="fact1, fact2,...")
parser.add_argument("-p","--pows", type=str, help="power1, power2,...")
parser.add_argument("-d","--ders", type=str, help="order-derivative1, order-derivative2,...")
parser.add_argument("-m","--ops", type=str, default=None, help="op1, op2, ... (optional if given in opnames)")
parser.add_argument("-c","--cols", type=str, default="all", help="Column indexes (default = all; first column=0)")
parser.add_argument("--scol", type=int, default=1, help="Column index for sload file (default = 1; first column=0)")
parser.add_argument("--acol", type=str, default="all", help="Column indexes to which mathematical operations will be applied (default = all; first column=0)")
parser.add_argument("-n","--nx", type=int, help="Number of interpolation points")
parser.add_argument("--xgrid", type=str, default=None, help="Filename containing the xgrid in the first column or sequence of x values (for interpolation)")
parser.add_argument("--norm", type=float, default=None, help="Normalize to number")
parser.add_argument("--xmin", type=float, default=None, help="xmin")
parser.add_argument("--xmax", type=float, default=None, help="xmax")
parser.add_argument("--tmax", type=float, default=None, help="Filter columns whose max value is lower than tmax")
parser.add_argument("-w","--incf", action='store_true', help="Include operated data to file")
parser.add_argument("-i","--inplace", action='store_true', help="In place writing")
parser.add_argument("-e","--exrows", type=str, default=None, help="Exclude the rows defined by the sequence of row indexes")
parser.add_argument("-z","--zeros", action='store_true', help="Consider zeros for interpolation outside x range")
parser.add_argument("-g","--gcom", action='store_true', help="Take the common points of the grid")
parser.add_argument("--gind", action='store_true', help="Take the indexes as grid")
parser.add_argument("-r","--rev", action='store_true', help="Reverse the order of the abscissa")
parser.add_argument("-k","--rhdr", type=int, default=0, help="Number of inital lines to read (header)")
parser.add_argument("-t","--trans", action='store_true', help="Transpose the matrix when using sloadtxt")
parser.add_argument("-o","--oname", type=str, default=0, help="Output name")
parser.add_argument("-v","--verbose", type=int, default=0, help="Level of verbosity")
args = parser.parse_args()

np.set_printoptions(precision=15)
availops = {'+' : np.add, '-' : np.subtract, '*' : np.multiply, '/' : np.divide, '%' : np.mod}

def checkparargs(arg,val):
     
    if getattr(args, arg) == None:
        var = [val for i in range(nf)]
    else:
        if arg == "ops":
            var = getattr(args, arg).split(",")
            ns = len(var)
        else:
            var = [float(i) for i in getattr(args, arg).split(",")]
            ns = len(var)
        if ns != nf:
            if ns == 1:
                for i in range(nf-1):
                    var.append(var[0])
            else:
                print("Error! Number of elements in {} is different from the number of files: ns, nf = {}, {}".format(arg, ns, nf))
                sys.exit()
            
    return var

if args.ops == None:
    if len(args.opnames) % 2 == 1:
        if not args.opnames[0] in availops.keys():
            args.opnames = ["+"]+args.opnames
        else:
            print("Error! The number of operators should be equal to the number of filenames!")
            sys.exit()

    ops = [i for i in args.opnames[0::2]]
    fnames = [i for i in args.opnames[1::2]]
    nf = len(fnames)
    
else:
    fnames = args.opnames
    nf = len(fnames)
    ops = checkparargs("ops","+")

for op in ops:
    if not op in availops.keys():
        print("Error! Bad option for argument op: ", op)
        sys.exit()

if args.oname:
    oname = args.oname
else:
    for i in range(nf):
        sname = fnames[i].split("_")
        if i == 0:
            oname = sname.copy()
        for j in range(len(sname)):
            if sname[j] != oname[j]:
                oname[j] = "-"
    k = 0
    for i in oname:
        if i == "-":
            k += 1
    if k == len(oname):
        print("Error! All fields of filenames are differents!")
        sys.exit()
    oname = "_".join(oname)

print("* Outname: ", oname)

if args.acol == "all":
    acol = [[0] for i in range(nf)]
else:
    acol = sequify(args.acol,ntot=nf)
    if not isinstance(acol[0], list):
        acol = [[i] for i in acol]
    if len(acol) != nf:
        print("Error! Inconsistent number of columns:")
        print(acol)
        exit()

shifts = checkparargs("shifts",0.0)
xfacts = checkparargs("xfacts",1.0)
adds = checkparargs("adds",0.0)
facts = checkparargs("facts",1.0)
pows = checkparargs("pows",1.0)
ders = [int(i) for i in checkparargs("ders",0)]
if args.cols != "all":
    cols = sequify(args.cols)
    if nf == 1:
        cols = [cols]
    else:
        if len(cols) != nf:
            print("Error! Inconsistent number of columns!")
            exit()
        if not isinstance(cols[0], list):
            cols = [[i] for i in cols]

operations=""
for i in range(nf):
    operations = operations+ops[i]+"[("+fnames[i]+"{*"+str(xfacts[i])+"+"+str(shifts[i])+"}"+ders[i]*"'"+"^"+str(pows[i])+")"+"*"+str(facts[i])+"+"+str(adds[i])+"]"

print("* Operations: ", operations)

nxlim = 100000

def derf(x,y,dord):
    
    der = y
    for o in range(dord):
        der = np.gradient(der,x)
            
    return der

curdir=os.getcwd()
for i in range(nf):
    if fnames[i] == "x":
        if i == 0:
            print("Error! Bad option for argument fnames: the option x cannot be in first position!")
            sys.exit()


nl = [0 for i in range(nf)]
nc = [0 for i in range(nf)]
mat1 = []
hdf = [None for i in range(nf)]
header = [None for i in range(nf)]
coms = [None for i in range(nf)]
for i in range(nf):
    if fnames[i] == "x":
        for j in range(nc[i]):
            mat1[i,0:nl[i],j] = mat1[0,0:nl[i],0]
    else:
        fform, dat, header[i], coms[i] = sloadtxt(fnames[i],col=args.scol)
        if not fform:
            f = open(fnames[i], 'r')
            for j in range(args.rhdr):
                hdf[i] = hdf[i]+f.readline()
            f.close()
            dat = np.loadtxt(fnames[i],skiprows=args.rhdr)
            dshape = getnlnc(fnames[i],skipcoms=True)
            dat = dat.reshape(dshape)
            #if args.xmin != None:
                #dat = dat[dat[:,0]>args.xmin]
            #if args.xmax != None:
                #dat = dat[dat[:,0]<args.xmax]
        if args.trans:
            dat = dat.transpose()
        if args.verbose > 1:
            print("* Opening {} with shape: {}".format(fnames[i],dat.shape))

        sind = dat[:,0].argsort()   # Sorting array
        mat1.append(dat[sind])
        #nl[i], nc[i] = mat1[i].shape
    
    mat1[i][:,0] = mat1[i][:,0]*xfacts[i]+shifts[i]
    for c in range(1,len(mat1[i][0])):
        if 0 in acol[i] or c in acol[i]:
            mat1[i][:,c] = facts[i]*np.power(derf(mat1[i][:,0],mat1[i][:,c],ders[i]),pows[i])
            mat1[i][:,c] = mat1[i][:,c]+adds[i]
    
    if args.xmin != None:
        mat1[i] = mat1[i][mat1[i][:,0]>args.xmin]
    if args.xmax != None:
        mat1[i] = mat1[i][mat1[i][:,0]<args.xmax]
    
    nl[i], nc[i] = mat1[i].shape

    if args.rev:
        mat1[i] = np.flip(mat1[i],axis=0)

print("* nls = ", *nl)
print("* ncs = ", *nc)

if args.cols == "all":
    for i in range(nf):
        if nc[i] != nc[0]:
            print("Error! Cannot assign automatically the number of columns to be proceeded!")
            sys.exit()
    cols = [[0] for i in range(nf)]
    for i in range(nf):
        for j in range(ncc):
            cols[i][j] = j+1

for i in range(nf):
    if len(cols[i]) == 1:
        if cols[i][0] == 0:
            cols[i] = [j for j in range(1,nc[i])]
    else:
        if 0 in cols[i]:
            print("Error! Column indexes cannot contain a sequence with a zero!")
            exit()

ncc = len(cols[0])
for i in range(nf):
    if len(cols[i]) != ncc:
        print("Error! Inconsistent sequences of column indexes:")
        print(len(cols[i]), ncc)
        exit()
    
for i in range(nf):
    
    j = min(cols[i])
    if  j <= 0:
        print("Error! The selected column index too low: i, j = {}, {}".format(i, j,))
        sys.exit()
    
    j = max(cols[i])
    if  j >= nc[i]:
        print("Error! The selected column index too high: i, j, nc[i] = {}, {}, {}".format(i, j, nc[i]))
        sys.exit()
    
    print(i, cols[i])

iTest = False

if args.xgrid != None:
    iTest = True
    xmin = min(mat1[0][:,0])
    xmax = max(mat1[0][:,0])
    try:
        xgrid = np.loadtxt(args.xgrid)
    except:
        try:
            xgrid = np.array(sequify(args.xgrid))
        except:
            print("Error! Cannot load grid file or grid sequence!")
            exit()
    if len(xgrid.shape) > 1:
        xgrid = xgrid[:,0]
    xgrid = xgrid[(xgrid>=xmin)&(xgrid<=xmax)]
    nx = len(xgrid)
    
    if nx == 0:
        print("Error! There is no points in the grid!")
        sys.exit()
    xmin = min(xgrid)
    xmax = max(xgrid)

else:
    if nf == 1:
        if args.nx == None:
            nx = nl[0]
        else:
            nx = args.nx
            iTest = True
        xmin = min(mat1[0][:,0])
        xmax = max(mat1[0][:,0])
    else:
        
        if args.gind:
            for i in range(1,nf):
                if nl[i] != nl[0]:
                    print("Error! The grids must have the same size when using gind option!")
                    sys.exit()
        else:
            for i in range(1,nf):
                if not np.array_equal(mat1[i][:,0],mat1[i-1][:,0]):
                    iTest = True
                    break
        
        if args.nx == None:
            nx = max(nl)
        else:
            nx = args.nx
            iTest = True
        
        if args.gcom:
            grid = mat1[0][:,0]
            for i in range(1,nf):
                grid = np.intersect1d(mat1[i][:,0],grid)
            nx = len(grid)
            if nx == 0:
                print("Error! The grids do not have any points in common!")
                sys.exit()
            xmin = min(grid)
            xmax = max(grid)
                
        else:
            xmins = [0.0 for i in range(nf)]
            xmaxs = [0.0 for i in range(nf)]
            for i in range(nf):
                xmins[i] = min(mat1[i][:,0])
                xmaxs[i] = max(mat1[i][:,0])
            if args.zeros:
                xmin = min(xmins)
                xmax = max(xmaxs)
            else:
                xmin = max(xmins)
                xmax = min(xmaxs)
        
        if nx > nxlim:
            print("Error! Nx parameter is too large: nx, nxlim = {}, {}".format(nx, nxlim))
            sys.exit()
    
    if iTest:
        xgrid = np.linspace(xmin,xmax,nx)
        
if args.verbose == 1:
    #print("*  xmins = ", *xmins)
    #print("*  xmaxs = ", *xmaxs)
    for j in range(ncc):
        xmaxs = np.zeros(nf)
        ymaxs = np.zeros(nf)
        yints = np.zeros(nf)
        for i in range(nf):
            xmaxs[i] = mat1[i][np.argmax(mat1[i][:,cols[i][j]]),0]
            ymaxs[i] = np.max(mat1[i][:,cols[i][j]])
            yints[i] = simps(mat1[i][:,cols[i][j]],mat1[i][:,0])
        print("\n* ic = {}, largest ymaxs: ".format(cols[i][j]))
        for i in np.argsort(-ymaxs):
            print("   - ({:7.3f}% ; x={:13.9f}, y={:13.9f}) {}".format(100.*ymaxs[i]/np.sum(ymaxs), xmaxs[i], ymaxs[i], fnames[i]))
        print("\n* ic = {}, largest yints: ".format(cols[i][j]))
        for i in np.argsort(-yints):
            print("   - ({:7.3f}% ; {:13.9f}) {}".format(100.*yints[i]/np.sum(yints), yints[i], fnames[i]))
           
print("* xmin = ", xmin)
print("* xmax = ", xmax)
print("* ncc = ", ncc)

if xmin > xmax:
    print("Error! xmin > xmax")
    sys.exit()

if args.verbose > 2:
    for i in range(nf):
        for j in range(1, ncc+1):
            print("* File {}, row 1 col {} : {}".format(fnames[i],cols[i][j-1], mat1[i][0,cols[i][j-1]]))

mat2 = np.zeros((nx,ncc+1),dtype=float)
if iTest:
    
    if args.gcom:
        print("-> Taking the common grid...")
        mat0 = []
        mat2[0:nx,0] = grid
        for i in range(nf):
            mat0.append(np.zeros((nx,ncc+1)))
            mat0[i][:,0] = grid
            for j in range(1, ncc+1):
                k = 0
                l = 0
                for x in mat1[i][:,0]:
                    if x in grid:
                        mat0[i][k,j] = mat1[i][l,cols[i][j-1]]
                        k += 1
                    l += 1
                mat2[:,j] = availops[ops[i]](mat2[:,j],mat0[i][:,j])
    else:
        print("-> Interpolation is performed...")
        mat2[0:nx,0] = xgrid
        mat0 = []
        for i in range(nf):
            mat0.append(np.zeros((nx,ncc+1)))
            mat0[i][:,0] = mat2[:,0]
            for j in range(1, ncc+1):
                if args.zeros:
                    funk = interp1d(mat1[i][:,0], mat1[i][:,cols[i][j-1]], kind='cubic',bounds_error=False,fill_value=0.)
                else:
                    funk = interp1d(mat1[i][:,0], mat1[i][:,cols[i][j-1]], kind='cubic')
                mat0[i][:,j] = funk(mat2[:,0])
                mat2[:,j] = availops[ops[i]](mat2[:,j],mat0[i][:,j])
else:
    mat2[:,0] = mat1[0][:,0]
    mat0 = []
    for i in range(nf):
        mat0.append(np.zeros((nx,ncc+1)))
        mat0[i][:,0] = mat2[:,0]
        for j in range(1, ncc+1):
            mat0[i][:,j] = mat1[i][:,cols[i][j-1]]
            mat2[:,j] = availops[ops[i]](mat2[:,j],mat1[i][:,cols[i][j-1]])

if args.gind:
    mat2[:,0] = np.arange(1,nl[0]+1)

if args.exrows != None:
    eseq = sequify(args.exrows,ordu=-1,nel=nx)
    for i in eseq:
        mat2 = np.delete(mat2,i,0)
    nx = len(mat2)


if args.norm != None:
    for j in range(1, ncc+1):
        ymax = np.max(mat2[:,j])
        mat2[:,j] = args.norm*mat2[:,j]/ymax
        for i in range(nf):
            mat0[i][:,j] = args.norm*mat0[i][:,j]/ymax




def update_header(nf,nc,header,icols):
    for i in range(nf):
        header0 = header[i].split("\n")
        for j in range(len(header0)):
            cvals = header0[j].split()
            if len(cvals) == nc[i]+1:
                header0[j] = " ".join(cvals[0:2]+[cvals[k+1] for k in icols[i]])
                header[i] = "\n".join(header0)
                break
    
    return header

header = update_header(nf, nc, header, cols)

if args.tmax:
    icols = list(range(1,ncc+1))
    k = 0
    for j in range(ncc,0,-1):
        ymax = np.max(mat2[:,j])
        if ymax < args.tmax:
            k += 1
            mat2 = np.delete(mat2,j,1)
            icols.remove(j)
    
    print("* Number of excluded columns: ", k)
    icols = [icols for i in range(nf)]
    lnc = [ncc+1 for i in range(nf)]
    header = update_header(nf, lnc, header, icols)
            

if args.verbose == 1:
    print("* Average over the interval {} - {}:".format(xmin, xmax))
    for j in range(1, ncc+1):
        yint = simps(mat2[:,j],mat2[:,0])
        #funk = interp1d(mat2[:,0], mat2[:,j], kind='cubic')
        #yint, err = quad(funk, xmin, xmax)
        print("  - ic = {} : int, avg1, avg2 = {}, {}, {}".format(j, yint, yint/(xmax-xmin), np.sum(mat2[:,j])/nx))

if oname in fnames:
    if not args.inplace:
        if args.verbose:
            sys.exit()
        print("Warning! Outname is identical as one of the inputnames: no output will be produced!")
        fop = sys.stdout
    else:
        print("Warning! Overwriting {}? (y/n)".format(oname))
        choice = input('> ')
        if choice != "y":
            sys.exit()
        fo = open(oname,"w")
        fop = fo
else:
    fo = open(oname,"w")
    fop = fo

hdf = list(set(hdf))
header = list(set(header))
coms = list(set([tuple(i) for i in coms]))


if len(hdf) == 1 and hdf[0] != None:
    print(hdf[0].strip(), file=fop)
else:
    if len(header) == 1 and header[0] != None and header[0] != "":
        print(header[0], file=fop)

if fform:
    for j in range(1, ncc+1):
        print("# ", coms[0][j-1], file=fop)
        for i in range(nx):
            print(mat2[i,0], mat2[i,j], file=fop)
        print(file=fop)
else:
    if args.incf:
        print("# ", oname, file=fop)
    elif len(coms) == 1 and coms[0] != None and coms[0] != ():
        print("# ", coms[0][0], file=fop)
    for j in range(nx):
        print(*mat2[j,:], file=fop)
    if args.incf:
        print(file=fop)
        for i in range(nf):
            print("#{} {}".format(i+1,fnames[i]), file=fop)
            for j in range(nx):
                print(*mat0[i][j,0:ncc+1], file=fop)
            print(file=fop)
